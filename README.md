# kanban-board-jobseeker
A Kanban board display for Jobseeker Pivotal Tracker stories (single html page, runs locally, uses pivotal API)

It's a single html page that runs locally.
It asks for your Pivotal API key, and it builds a URL that contains it i.e. it's not stored anywhere, or accessible to anyone else.
You can select a Pivotal project, and a Label/Epic, and then you can bookmark at any stage.

Access the Board at: http://mycf.sg.gitlab.io/kanban-board-jobseeker/index.html

Fork from: https://github.com/tomcoombs/kanban-board-for-pivotal
